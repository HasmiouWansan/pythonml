# Include datas
dataSet_train = "data/fr_gsd-ud-train.conllu"
dataSet_test = "data/fr_gsd-ud-test.conllu"
dataSet_dev = "data/fr_gsd-ud-dev.conllu"

#  Write a function that returns the number of words in the GSD corpus (i.e. the number of tokens).
def countToken(data):
    n = 0
    for line in open(data, "r"):

        if line.startswith("#"):
            continue

        if "-" in line.split("\t")[0]:
            continue

        if not line.strip():
            continue
        n =+1
    return n

print("le nombre de token (standard version) est: ", countToken(dataSet_train))

# Pythonic Version
def countToken_py(data):
    return sum(1 for line in open(data, "r") if not any([ \
        line.startswith("#"),
        "-" in line.split("\t")[0],
        not line.strip()]))

#nbToken = getTotalToken_py(dataSet_train)
print("le nombre de token (pythonic version) TRAIN est:", countToken_py(dataSet_train))
print("le nombre de token (pythonic version) DEV est:", countToken_py(dataSet_dev))
print("le nombre de token (pythonic version) TEST est:", countToken_py(dataSet_test))

# Write a function that returns the number of unique words (i.e. the number of types1)
def countUnicWord(data):
    return len({line.split("\t")[1] for line in open(data, "r") if not any([ \
        line.startswith("#"),
        "-" in line.split("\t")[0],
        not line.strip()])})
print("Nombre de mot unique pour la phrase1-TRAIN: ", countUnicWord(dataSet_train))
print("Nombre de mot unique pour la phrase1-DEV: ", countUnicWord(dataSet_dev))
print("Nombre de mot unique pour la phrase1-TEST: ", countUnicWord(dataSet_test))

# Question 1: Write a function that returns a list of all the sentences contained in a conllu ﬁle.
def read_sentences(infile):
    # Notez que cette fonction lit tout le fichier en mémoire qui est
    # généralement pas une bonne idée (le fichier peut être très volumineux)
    data = infile.read()

    # do not return an empty sentence at the end of file
    return [d for d in data.split("\n\n") if d.strip()]

def stream_sentences(infile):
    data = []
    for line in infile:
        if not line.strip():
            yield "\n".join(data)
            data = []
            continue
        data.append(line.strip())
    if data:
        yield "\n".join(data)

print(f"#sentences: {len(read_sentences(open('data/fr_gsd-ud-train.conllu'))):,}")

assert read_sentences(open(dataSet_train))[0] == next(stream_sentences(open(dataSet_train, "r")))
assert read_sentences(open(dataSet_train))[-1] == list(stream_sentences(open(dataSet_train, "r")))[-1]

s = read_sentences(open(dataSet_train))[0]
print(s)

# Question 2: number of verbs and passive verbs in the sentence
def count_verbs(sentence):
    c = Counter(line.split("\t")[7] for line in sentence.split("\n") if not any([ \
        line.startswith("#"),
        "-" in line.split("\t")[0]]))

    return sum(v for k, v in c.items() if "nsubj" in k), c["nsubj:pass"]