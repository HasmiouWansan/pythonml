## UdProject
L' UD_French-GSD a été converti en 2015 à partir de la version de contenu de la banque de dépendances universelles v2.0 ( https://github.com/ryanmcd/uni-dep-tb ). Il est mis à jour depuis 2015 indépendamment de la source précédente.

La version 2.4 des données UD_French-GSD comprend 400 387 mots (16 342 phrases). L'identifiant des phrases étaient disponible dans la ressource d' origine, si nouveau sent_id ont été automatiquement introduit dans le corpus converti avec des préfixes fr-ud-train, fr-ud-dev et fr-ud-test sur les fichiers originaux correspondants, suivi d'un numéro à 5 chiffres suivant l'ordre des phrases.

Pour répondre aux exigences de taille des données de test de 10K mots, une partie du fichier de développement original a été déplacée dans le fichier de test! Depuis la version 2.0, le fractionnement des données est:
	<ul>
	    <li>fichier fr-ud-train.conll: 14 450 phrases; 354 655 mots</li>
	    <li>fr-ud-train_00001 à fr-ud-train_14554</li>
	    <li>fichier fr-ud-dev.conll: 1 478 phrases; 35 714 mots</li>
	    <li>fr-ud-dev_00001 à fr-ud-dev_01478</li>
	    <li>fichier fr-ud-test.conll: 416 phrases; 10.018 mots</li>
	    <li>fr_ud-test_00001 à fr_ud-test_00298</li>
	    <li>fr-ud-dev_01479 à fr-ud-dev_01596</li>
	</ul>

Les phrases sont mélangées et il n'y a aucun moyen de savoir quelle est la source ou le genre d'une phrase donnée.
Probablement à cause d'un bogue dans un programme de conversion, la version 1.2 contient de nombreuses phrases tronquées (date manquante par exemple). Presque toutes les phrases tronquées proviennent de Wikipedia, il était donc possible de récupérer le texte original. La plupart des phrases tronquées ont été complétées dans la version 1.3. Certaines phrases ont été complétées plus tard. Il y a probablement encore des phrases tronquées.
